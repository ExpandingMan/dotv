from libqtile.command.client import InteractiveCommandClient
from ptpython.repl import embed

cmd = InteractiveCommandClient()


# it's better to do ptpython -i repl.py otherwise it won't load config
#embed(globals(), locals())
