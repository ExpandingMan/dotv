#!/bin/bash
$HOME/.screenlayout/default.sh
$HOME/sbin/bgrandom &
picom --config $HOME/.config/picom/picom.conf &
blueman-applet &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/bin/gnome-keyring-daemon --start &
nm-applet &
