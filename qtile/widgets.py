import os
import re
import subprocess

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.log_utils import logger

from utils import *
from constants import *


def _volumestring(v):
    if v > 66:
        o = "  "
    elif v > 33:
        o = " 墳"
    elif v > 0:
        o = "  "
    else:
        o = "  "
    o = styledtext(o, foreground=colors["background"], background=colors["cyan"])
    return o + styledtext(f" {v}% ", foreground=colors["foreground"], background=colors["selection"])

def volumestring():
    p = subprocess.run(["pamixer", "--get-volume-human"], stdout=subprocess.PIPE)
    s = p.stdout.decode().strip()
    if s.startswith("mute"):
        return styledtext(" 婢", background=colors["pink"], foreground=colors["background"])
    elif s.endswith("%"):
        s = s.rstrip("%")
        if s.isdigit():
            return _volumestring(int(s))
        else:
            return styledtext("  ", background=colors["red"], foreground=colors["background"])
    else:
        return styledtext("  ", background=colors["red"], foreground=colors["background"])

def defaultgroupbox():
    return widget.GroupBox(
            this_current_screen_border=colors["purple"],
            this_screen_border=colors["cyan"],
            other_current_screen_border=colors["yellow"],
            other_screen_border=colors["comment"],
            active=colors["comment"],
            highlight_color=colors["cyan"],
            highlight_method="line",
            urgent_border=colors["red"],
            urgent_text=colors["background"],
            padding_x=8,
            spacing=0,
            margin_x=0,
            borderwidth=3,
            hide_unused=True,
            )

def defaultwindowname():
    return widget.TaskList(
            borderwidth=1,
            highlight_method="block",
            max_title_width=256,
            urgent_border=colors["red"],
            foreground=colors["selection"],
            border=colors["background"],
            unfocused_border=colors["background"],
            markup_focused=styledtext("", foreground=colors["purple"]),
            markup_floating="",
            markup_normal="",
            markup_maximized="",
            )

def defaultprompt():
    return widget.Prompt(
            cursor_color=colors["foreground"],
            markup=True,
            prompt="",
            fmt=styledtext("▮◗ ", foreground=colors["cyan"])+"{}"+styledtext("▯",
                foreground=colors["green"]),
            padding=18,
            )

def defaultclock():
    return widget.Clock(
            format="%Y-%m-%d %H:%M:%S",
            fmt=styledtext("◖ ", foreground=colors["comment"])+"{}"+styledtext(" ▮",
                foreground=colors["yellow"])+styledtext("◗", foreground=colors["comment"]),
            padding=18,
            )

def defaultcpugraph():
    return [
            widget.TextBox(styledtext("", size="large"),
                foreground=colors["background"],
                background=colors["purple"],
                padding=8,
                ),
            widget.CPUGraph(
                graph_color=colors["yellow"], fill_color=colors["selection"],
                border_color=colors["purple"],
                margin_x=0,
                margin_y=0,
                border_width=2, line_width=1, type="linefill"
                )]

#TODO: fixed width so it doesn't bound around, look up python format string syntax
def defaultmemorygraph():
    return [widget.Memory(measure_mem="G", background=colors["comment"],
        format=styledtext("", size="large")+" {MemUsed:.1f} {mm}B/{MemTotal:.1f} {mm}B ",
        foreground=colors["background"],
        margin_x=0, margin_y=0,
        padding=8,
        ),
        widget.MemoryGraph(
            graph_color=colors["yellow"], fill_color=colors["selection"],
            border_color=colors["comment"],
            margin_x=0, margin_y=0,
            border_width=2, line_width=1, type="linefill",
            ),
        ]

def defaultvolume():
    return widget.Volume(
            step=5,
            background=colors["cyan"],
            foreground=colors["background"],
            fmt="  {} ",
            padding=0,
            update_interval=0.2,
            get_volume_command="pamixer --get-volume-human",
            volume_up_command="fish -c 'tvctl-audio -i 5'",
            volume_down_command="fish -c 'tvctl-audio -d 5'",
            mouse_callbacks={"Button3": lazy.spawn("tvu named audio easyeffects")},
            )

def defaultsep():
    return widget.Sep(linewidth=1, padding=0, size_percent=100, foreground=colors["comment"])

def defaultwifi():
    if not haswifi():
        return []
    return [widget.Wlan(interface=get_network_interface(), format="{essid} {percent:5.0%}",
        disconnected_mesage="",
        background=colors["selection"],
        fmt=styledtext("  ", background=colors["green"], foreground=colors["background"],
            size="large")+
        " {} ",
        padding=0,
        margin=0,
        update_interval=2,
        )
        ]

# not really great to decide this in this function but pain in the ass otherwise
def defaultbattery():
    if not hasbattery():
        return []
    return [widget.Spacer(length=12),
            widget.BatteryIcon(background=colors["yellow"]),
            widget.Battery(background=colors["selection"],
                format=" {percent:2.0%} ",
                notify_below=20,
                )
            ]


def tvmenu():
    return [widget.TextBox(text="≣",
                           mouse_callbacks={"Button1": lazy.spawn("chmenu"),
                                            "Button3": lazy.spawn("utilmenu"),
                                            },
                           foreground=colors["background"],
                           background=colors["yellow"],
                           padding=10,
                           )
            ]
