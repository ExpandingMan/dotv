import os
import re
import subprocess

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.log_utils import logger

from utils import *
from constants import *


@lazy.function
def togroup(qtile, g):
    """
        togroup(qtile, g)

    Goes to group `g` on whatever screen it's currently on.
    """
    gg = qtile.groups_map[g.name]
    scr = gg.info()["screen"]
    if gg.info()["screen"] == None:
        scr = qtile.current_screen.index
    qtile.cmd_to_screen(scr)
    gg.cmd_toscreen()

@lazy.function
def group_to_next_screen(qtile):
    gg = qtile.current_group
    qtile.cmd_next_screen()
    gg.cmd_toscreen()

@lazy.function
def group_to_prev_screen(qtile):
    gg = qtile.current_group
    qtile.cmd_next_screen()
    gg.cmd_toscreen()

def _keymap_group(g, k):
    return [
            # mod1 + letter of group = switch to group
            Key([mod], k,
                lazy.group[g.name].toscreen(),
                #togroup(g),
                desc=f"Switch to group {g.name}",
                ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key([mod, "shift"], k,
                lazy.window.togroup(g.name, switch_group=False),
                desc=f"Switch to & move focused window to group {g.name}",
                ),
            ]

def keymap_group(g):
    if g.name in "1 2 3 4 5 6 7 8 9 0".split():
        return _keymap_group(g, g.name)
    elif g.name == "top":
        return _keymap_group(g, "q")
    elif g.name == "browser":
        return _keymap_group(g, "w")
    elif g.name == "games":
        return _keymap_group(g, "e")
    elif g.name == "video":
        return _keymap_group(g, "r")
    else:
        return []

def keymap(groups=[]):
    o = [
            # movement
            Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
            Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
            Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
            Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
            Key([mod], "x", lazy.next_urgent(), desc="Next urgent window"),

            # moving windows
            Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
            Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
            Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
            Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

            # background
            Key([mod, "shift"], "b", lazy.spawn(os.path.expanduser("~/sbin/bgrandom")),
                desc="shuffle background"),

            # manipulating windows
            Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
            Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
            Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
            Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
            Key([mod], "u", lazy.layout.normalize(), desc="Reset all window sizes"),
            Key([mod], "space", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen"),

            # layouts
            Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),

            # shourtcuts
            Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
            Key([mod, "shift"], "Return", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
            Key([mod], "z", lazy.spawn("rofi -show drun -1"), desc="bring up rofi menu"),
            Key([mod, "shift"], "d", lazy.spawn("rofi -show ssh -1"), desc="rofi ssh menu"),
            Key([mod], "backslash", lazy.spawn("rofi -show window -1"), desc="rofi window menu"),
            Key([mod], "y", lazy.spawn(os.path.expanduser("~/sbin/top")), desc="system panel"),
            Key([mod, "shift"], "m", lazy.spawn(os.environ["BROWSER"]+" "+moonlander_url),
                desc="show moonlander config"),
            Key([mod], "Print", lazy.spawn("flameshot gui"), desc="open flameshot GUI"),
            Key([mod, "shift"], "Print", lazy.spawn("flameshot screen -c"),
                desc="current screen to clipboard with flameshot"),
            Key([mod, "control", "shift"], "Print", lazy.spawn("flameshot full -c"),
                desc="all screens to clipboard with flameshot"),

            # config
            Key([mod], "Escape", lazy.window.kill(), desc="Kill focused window"),
            KeyChord([mod, "shift"], "c", [
                Key([], "c", lazy.reload_config(), desc="Reload the config"),
                Key([], "r", lazy.restart(), desc="restart Qtile in place"),
                ]),
            KeyChord([mod, "shift"], "Escape", [
                Key([], "l", lazy.spawn("fish -c 'fexit lock'")),
                Key([], "s", lazy.spawn("fish -c 'fexit suspend'")),
                Key([], "r", lazy.spawn("fish -c 'fexit reboot'")),
                Key(["shift"], "s", lazy.spawn("fish -c 'fexit shutdown'")),
                Key([], "e", lazy.shutdown()),
                ], name="(l)ock; (s)uspend; (S)hutdown; (e)xit; (r)eboot", mode=False),

            # screens
            Key([mod], "Up", lazy.next_screen(), desc="go to next screen"),
            Key([mod], "Right", lazy.next_screen(), desc="go to next screen"),
            Key([mod], "Down", lazy.prev_screen(), desc="go to previous screen"),
            Key([mod], "Left", lazy.prev_screen(), desc="go to previous screen"),
            #Key([mod], "a", lazy.to_screen(0), desc="go to screen 1"),
            #Key([mod], "s", lazy.to_screen(1), desc="go to screen 2"),
            #Key([mod], "d", lazy.to_screen(2), desc="go to screen 3"),
            #Key([mod], "f", lazy.to_screen(3), desc="go to screen 4"),

            Key([mod], "period", group_to_next_screen(), desc="move group to next screen"),
            Key([mod], "comma", group_to_prev_screen(), desc="move group to previous screen"),

            # notifications
            Key([mod], "n", lazy.spawn("dunstctl close"), desc="close notification"),
            Key([mod, "shift"], "n", lazy.spawn("dunstctl close-all"), desc="close all \
                notifications"),
            Key([mod, "control"], "n", lazy.spawn("dunstctl action 0"), desc="default notification \
                action"),

            # audio
            Key([], "XF86AudioRaiseVolume",
                lazy.spawn("fish -c 'tvctl-audio -i 5'"),
                desc="Raise volume"),
            Key([], "XF86AudioLowerVolume",
                lazy.spawn("fish -c 'tvctl-audio -d 5'"),
                desc="Lower volume"),
            Key([], "XF86AudioMute", lazy.spawn("fish -c 'tvctl-audio -t'"), desc="Toggle audio mute"),

            # more TV-specific bindings
            Key(["mod1"], "F4", lazy.window.kill()),
            Key(["mod1"], "Left", lazy.screen.prev_group()),
            Key(["mod1"], "Right", lazy.screen.next_group()),
            Key(["mod1"], "Up", lazy.window.toggle_fullscreen()),
            Key([], "XF86HomePage", lazy.spawn("chmenu")),
            Key([mod], "d", lazy.spawn("chmenu")),
            Key([], "Menu", lazy.spawn("rofi -show drun")),
            Key([], "XF86AudioNext", lazy.screen.next_group()),
            Key([], "XF86AudioPrev", lazy.screen.prev_group()),

            Key([], "XF86AudioRewind", lazy.spawn("xdotool click --repeat 4 --delay 50 4")),
            Key([], "XF86AudioForward", lazy.spawn("xdotool click --repeat 4 --delay 50 5")),
            ]
    for g in groups:
        o.extend(keymap_group(g))
    return o

