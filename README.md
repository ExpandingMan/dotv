# dotv
(the name is from "dotfiles" and "tv")

This is a collection of tools to make it convenient to use a machine from a television with simple
"pointer remote" input.  This is mainly organized around using
[`qtile`](https://github.com/qtile/qtile) together with [`rofi`](https://github.com/davatorium/rofi)
to create dedicated workspaces that are easy to navigate.

## Prerequisites
- [`qtile`](https://github.com/qtile/qtile) (for window management)
- [`rofi`](https://github.com/davatorium/rofi) (for menus)
- [`fish`](https://fishshell.com/) (for scripts)

## Supporting Software
- my standard [dotfiles](https://gitlab.com/ExpandingMan/dotfiles) as a configuration base
- [`barrier`](https://github.com/debauchee/barrier) for remote control
- `libcec` for controlling the television, mostly through `cec-ctl` and `cec-client`.

## Hardware
I have a standard `x86` linux machine set up permanently on my TV.  Control is mostly achieved
through an "air mouse" (pointer hooked up to an accelerometer in a remote control) which are cheaply
available and solve most UI issues.  I also have a bluetooth "60%" mechanical keyboard in case it's
needed.

## Quirks and Troubleshooting
Some notes on issues I encountered can be found [here](/troubleshooting.md).

