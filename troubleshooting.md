
# Troubleshooting

## `lightdm`
Initially `lightdm` did not work on this machine at all.  Turns out this is because it boots so fast
that `Xorg` tries to start before the display is even available to it.  The solution to this is to
force `lightdm` to check for displays before starting `Xorg`, one can do this by setting
```
[LightDM]
logind-check-graphical=true
```
in `/etc/lightdm/lightdm.conf`.

Once this is done, enabling auto-login is straightforward, instructions can be found [on the Arch
wiki](https://wiki.archlinux.org/title/LightDM#Enabling_autologin).

