local WezTerm = require("wezterm")

return {
    enable_tab_bar = false,
    color_scheme = "DraculaCorrect",
    font = WezTerm.font("JuliaMono-Regular"),
    font_rules = {
        {italic=true, font=WezTerm.font("JuliaMono-MediumItalic")},
        {intensity="Bold", font=WezTerm.font("JuliaMono-Bold")},
        {intensity="Bold", italic=true, font=WezTerm.font("JuliaMono-BoldItalic")},
    },
    font_size = 24.0,
    window_padding = {
        left = 0,
        right = 0,
        top = 0,
        bottom = 0,
    },

    adjust_window_size_when_changing_font_size = false,

    disable_default_key_bindings = true,
    keys = {
        {mods="CTRL|SHIFT", key="c", action=WezTerm.action{CopyTo="Clipboard"}},
        {mods="CTRL|SHIFT", key="v", action=WezTerm.action{PasteFrom="Clipboard"}},
        {mods="ALT|SHIFT", key="=", action="IncreaseFontSize"},  -- this is ALT+
        {mods="ALT", key="-", action="DecreaseFontSize"},
        {mods="ALT", key="/", action=WezTerm.action{Search={CaseSensitiveString=""}}},
        {mods="ALT|SHIFT", key="r", action="ReloadConfiguration"},
        -- this is like vim normal mode, hence the key binding
        {mods="ALT", key="n", action="ActivateCopyMode"},
        {mods="ALT|CTRL", key="u", action=WezTerm.action{ScrollByPage=-1}},
        {mods="ALT|CTRL", key="d", action=WezTerm.action{ScrollByPage=1}},
        {mods="ALT", key="k", action=WezTerm.action{ScrollByLine=-1}},
        {mods="ALT|SHIFT", key="k", action=WezTerm.action{ScrollByLine=-5}},
        {mods="ALT", key="j", action=WezTerm.action{ScrollByLine=1}},
        {mods="ALT|SHIFT", key="j", action=WezTerm.action{ScrollByLine=5}},
    },

    key_tables = {
        copy_mode = {
            --NOTE alt mouse drag should also work for block selection
            {mods="NONE", key="Escape", action=WezTerm.action{CopyMode="Close"}},
            {mods="NONE", key="h", action=WezTerm.action{CopyMode="MoveLeft"}},
            {mods="NONE", key="j", action=WezTerm.action{CopyMode="MoveDown"}},
            {mods="NONE", key="k", action=WezTerm.action{CopyMode="MoveUp"}},
            {mods="NONE", key="l", action=WezTerm.action{CopyMode="MoveRight"}},
            {mods="NONE", key="w", action=WezTerm.action{CopyMode="MoveForwardWord"}},
            {mods="NONE", key="b", action=WezTerm.action{CopyMode="MoveBackwardWord"}},
            {mods="NONE", key="0", action=WezTerm.action{CopyMode="MoveToStartOfLine"}},
            {mods="NONE", key="v", action=WezTerm.action{CopyMode={SetSelectionMode="Cell"}}},
            {mods="CTRL", key="y", action=WezTerm.action{CopyTo="ClipboardAndPrimarySelection"}},
            {mods="NONE", key="y", action=WezTerm.action{Multiple={
                WezTerm.action{CopyTo="ClipboardAndPrimarySelection"},
                WezTerm.action{CopyMode="Close"},
            }}},
            {mods="CTRL", key="v", action=WezTerm.action{CopyMode={SetSelectionMode="Block"}}},
        }
    }
}
